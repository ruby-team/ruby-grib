def_path = File.join(File.expand_path(File.dirname(__FILE__)),"definitions")
if File.exist?(def_path)
  env_name = 'GRIB_DEFINITION_PATH'
  if gdp = ENV[env_name]
    ENV[env_name] = "#{gdp}:#{def_path}"
  else
    begin
      path = `grib_info -d`.strip
    rescue
      $stderr.print <<-EOM
The 'grib_info' command cannot be found.
If you have the command, please set the PATH environment variable.
      EOM
      exit(-1)
    end
    if File.exist?(path)
      ENV[env_name] = "#{path}:#{def_path}"
    end
  end
end
