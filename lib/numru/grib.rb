require "narray_miss"
require "date"
require "numru/grib/setenv.rb"
require "numru/grib/version.rb"
require "numru/grib.so"
require "numru/grib/grib.rb"

if defined?(NumRu::NArray)
  unless NumRu::Grib::SUPPORT_BIGMEM
    $stderr.print <<EOM
rb-grib was compiled with NArray but NumRu::NArray is used.
Check and use consistent narray!
EOM
    raise LoadError, "failed to load rb-grib"
  end
else
  if NumRu::Grib::SUPPORT_BIGMEM
    $stderr.print <<EOM
rb-grib was compiled with NumRu::NArray but NArray is used.
Check and use consistent narray!
EOM
    raise LoadError, "failed to load rb-grib"
  end
end
