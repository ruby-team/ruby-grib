require "mkmf"

na_path = RbConfig::CONFIG["sitearchdir"]
begin
  require "rubygems"
  gem = true
rescue LoadError
  gem = false
end
if gem
  if ( na_type = ENV["NARRAY_TYPE"] )
    nas = [na_type]
  else
    nas = %w(narray numru-narray)
  end
  if Gem::Specification.respond_to?(:find_by_name)
    nas.each do |na|
      begin
        if ( spec = Gem::Specification.find_by_name(na) )
          na_type = na
          na_path = spec.full_gem_path
          case na_type
          when "narray"
            na_path = File.join(na_path, "src")
          when "numru-narray"
            na_path = File.join(na_path, "ext", "numru", "narray")
          end
          break
        end
      rescue LoadError
      end
    end
  else
    nas.each do |na|
      if ( spec = Gem.source_index.find_name(na) ).any?
        na_type = na
        na_path = spec[0].full_gem_path
        case na_type
        when "narray"
          na_path = File.join(na_path, "src")
        when "numru-narray"
          na_path = File.join(na_path, "ext", "numru", "narray")
        end
        break
      end
    end
  end
end

dir_config("narray", na_path, na_path)
unless have_header("narray.h")
  $stderr.print "narray.h does not found. Specify the path.\n"
  $stderr.print "e.g., gem install rb-grib -- --with-narray-include=path\n"
  exit(-1)
end

dir_config("grib_api")
if have_header("grib_api.h") && have_library("grib_api")
  create_makefile("numru/grib")
end
