$LOAD_PATH.unshift File.expand_path(File.join('..','ext'), File.dirname(__FILE__))
$LOAD_PATH.unshift File.expand_path(File.join('..','lib'), File.dirname(__FILE__))
alias :_require :require
def require(arg)
  arg = "grib.so" if arg == "numru/grib.so"
  _require(arg)
end
require "numru/grib"
