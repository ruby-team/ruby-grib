# -*- mode: ruby; coding: utf-8; -*-
require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = 'spec/**/*_spec.rb'
end
