# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "numru/grib/version"

Gem::Specification.new do |s|
  s.name        = "rb-grib"
  s.version     = NumRu::Grib::VERSION
  s.authors     = ["Seiya Nishizawa"]
  s.email       = ["seiya@gfd-dennou.org"]
  s.homepage    = "http://ruby.gfd-dennou.org/products/rb-grib/"
  s.summary     = %q{Ruby class library to hanlde GRIB file}
  s.description = %q{This class library enable you to handle GRIB file.}

  s.rubyforge_project = "rb-grib"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]
  s.extensions << "ext/extconf.rb"

  # specify any dependencies here; for example:
  s.add_development_dependency "rspec"
  s.add_runtime_dependency "numru-narray"
  s.add_runtime_dependency "narray_miss"
end
